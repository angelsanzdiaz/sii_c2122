## [3.2] - 2021-11-24
### Added
- Añadido un bot para el jugador 1. ¡Una IA controla la raqueta!
- Al llegar a 3 puntos se acaba el juego
- Fin practica 3

## [3.1] - 2021-11-23
### Added
- Se ha añadido un logger que lleva el registro de la puntuación
## [2.2] - 2021-11-06
### Changed
- La pelota de juego ahora cambia de tamaño a lo largo del tiempo
## [2.1] - 2021-10-27
### Added
- Se ha implementado el movimiento tanto de la pelota como de las raquetas
- Adición del readme con los controles del juego
## [1.2] - 2021-10-10
### Added
- Se ha creado este Changelog para la documentación de los cambios que se van a realizar en el código de las prácticas de Sistemas Informáticos Industriales del curso 2021 - 2022
- Etiquetas que indican la autoría de las modificaciones
