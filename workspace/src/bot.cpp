#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(void){
	DatosMemCompartida *pdata;
	int fd;
	clock_t t;
	
	fd = open("mmap_bot", O_RDWR);
	pdata = (DatosMemCompartida *) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	
	while(1) {
		
	usleep(25000);
	//pelota por encima de la raqueta
	if(pdata -> esfera.centro.y > pdata -> raqueta1.y1)
		pdata -> accion = 1;

	//pelota por debajo de la raqueta
	else if(pdata -> esfera.centro.y < pdata -> raqueta1.y2)
		pdata -> accion = -1;
			
	     else
	     	pdata -> accion = 0;
	}
}

