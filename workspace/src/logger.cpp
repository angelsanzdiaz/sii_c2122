#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct data{
	int player;
	int score;
} data;

int main(int argc, char **argv){
	int fd;
	data data;
	
	//borramos FIFO por si existía previamente
	unlink("FIFO");
	
	//creamos el FIFO
	if(mkfifo("FIFO", 0600)<0){
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	//Abre el FIFO
	if((fd = open("FIFO", O_RDONLY))<0){
		perror("No puede abrirse el FIFO");
		return 1;
	}
	
	//leemos y escribimos en pantalla
	while (read(fd, &data , sizeof(data)) == sizeof(data)){
		fprintf(stdout, "Jugador %d ha marcado, lleva un total de %d puntos \n", data.player, data.score);
	}
	
	close(fd);
	unlink("FIFO");
	return 0;
}
		
